#!/bin/bash

####### Authors: Emon SHR and Sammay Sarker. ######

export STATE=${STATE:-0}

##### Place your STATE range in STATEVAL, always counted from 0. Every successfully executed function will increment STATE by 1.
STATEVAL=({0..2}) #we didn't add 0..3 in this context, bcos we need STATE=3 to pass as "job is completed" executed from "frame"
FUNC=({a..c}) #Place your function range here


function esam () {

##### Will handle main functions from a,b,c... to whatever needed #####
function frame () {
	while [[ "$STATE" -le "${#FUNC[@]}" ]]; do
	
	if [[ "$STATE" ==  "${STATEVAL[STATE]}" ]]; then
		if "${FUNC[STATE]}"; then
		continue
		
		else
		help >& 2
		break
		return 1
		fi
		
	else
		echo -e "\nesam Detected that you have completed your job. Want to do it again? "
		echo -e "Just run "esam-repeat"."
		break
	fi
	done
}


function help () {
	cat << EOF
		   
esam : Error occured !
		 After fix mentioned error, run esam again. 
		 It will automatically resume the automation.
	 
	   To manually resume automation from a specific part, follow this-
		 from a run esam --from-a
	   	 from b run esam --from-b
	   	 from c run esam --from-c
	  
	   if you want to run automation from beginning again-
	  	 run esam-repeat
	 
	   To run a specific part and skip others, follow this-
 	  	 esam --only-a #for the part a.
	   	 esam --only-b #for the part b.
	   	 esam --only-c #for the part c.
EOF

}

##### These functions will be Automated one by one. Place your all functions here named with a,b,c... to whatever needed #####
##### Don't forget to use '&& export STATE=NUMBER' with every new function, incremented NUMBER by 1. #####
function a () {
	ls ~/Pictures  && export STATE=1
	return
}

function b () {
	ls ~/Music  && export STATE=2
	return
}

function c () {
	ls ~/Downloads && export STATE=3
	return
}



##### BootStrap #####
#To make a start of STATE=0 we are using parameter substitution.

while [[ "$STATE" -le "${#FUNC[@]}" ]]; do   
	#NO parameter given from command line, just run the default automation
	if [[ -z "$1" ]]; then
	frame
	break
	
	elif #resume manually from a specific function, prior functions will be avoided
	[[ "$1" =~ ^\-\-from\-[a-c]$ ]]; then #add your function ranges here too '^\-\-from\-[a- ..]$'
	Arg="$1"
	val=$(echo "$Arg" | cut -c 8)
	
	if "$val"; then
		frame
		break
	else
		help >& 2
		break
		return 1
	fi
	
	
	elif #Don't resume anything just run the certain function and skip others
	[[ "$1" =~ ^\-\-only\-[a-c]$ ]]; then #add your function ranges here too '^\-\-from\-[a- ..]$'
	Arg2="$1"
	val2=$(echo "$Arg2" | cut -c 8)
	
	if "$val2"; then
		break

	else
		help >& 2
		break
		return 1

	fi
	
	else
	echo "esam error: "$1" is not a valid parameter." >&2
	help >& 2
break
return 1
	fi
	
done

}

# To repeat from beginning
function esam-repeat () {
export STATE=0 && esam
return
}

